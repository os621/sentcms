<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Widget;
use Think\Controller;

/**
 * 分类widget
 * 用于动态调用分类信息
 */

class AdWidget extends Controller{

	public function run($id){
		$place = D('AdPlace')->where(array('id'=>$id))->find();
		if (empty($place) || !$place) {
			echo "无此广告位！";return;
		}
		if ($place['status'] != '1') {
			echo "该广告位已关闭！";return;
		}
		$ad = D('Ad')->where(array('place_id'=>$id))->select();
		switch ($place['show_type']) {
			//幻灯片显示
			case '1':
				$template = $place['template'] ? $place['template'] : "sider";
				break;
			//对联广告
			case '2':
				$template = $place['template'] ? $place['template'] : "couplet";
				break;
			//图片列表广告
			case '3':
				$template = $place['template'] ? $place['template'] : "image";
				break;
			//图文列表广告
			case '4':
				$template = $place['template'] ? $place['template'] : "images";
				break;
			//文字列表广告
			case '5':
				$template = $place['template'] ? $place['template'] : "text";
				break;
			//代码广告广告
			case '6':
				$template = $place['template'] ? $place['template'] : "code";
				break;
			default:
				$template = $place['template'] ? $place['template'] : "default";
				break;
		}
		$data = array(
			'place' => $place,
			'ad' => $ad,
		);
		$this->assign($data);
		$this->display('Ad/'.$template);
	}
}