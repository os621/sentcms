<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use OT\DataDictionary;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController {

	//系统首页
    public function index(){

        $category = D('Category')->getTree();
        $lists    = D('Document')->lists(null);

        $this->assign('category',$category);//栏目
        $this->assign('lists',$lists);//列表
        $this->assign('page',D('Document')->page);//分页

        $this->setSeo(C('WEB_SITE_TITLE'));
        $this->display();
    }

    //系统单页
    public function page($id,$model='Page'){
        $data = D($model)->detail($id);

        if ($data['status'] == '-1') {
            $this->error("该文档已被删除！");
        }elseif($data['status'] == '0'){
            $this->error("该文档已被禁用！");
        }

        $this->assign('data',$data);//数据
        $this->setSeo($data['title'],$data['keyword'],$data['description']);
        $this->display();
    }
}