<?php

namespace Addons\Friendlink;
use Common\Controller\Addon;
use Think\Model;

/**
 * 友情链接插件
 * @author tpcms
 */

    class FriendlinkAddon extends Addon{

        public $info = array(
            'name'=>'Friendlink',
            'title'=>'友情链接',
            'description'=>'友情链接管理',
            'status'=>1,
            'author'=>'tpcms',
            'version'=>'0.1'
        );

        public $admin_list = array(
            'model'=>'Friendlink',		//要查的表
			'fields'=>'*',			//要查的字段
			'map'=>array('status'=>1),				//查询条件, 如果需要可以再插件类的构造方法里动态重置这个属性
			'order'=>'id desc',		//排序,
			'list_grid'=>array( 		//这里定义的是除了id序号外的表格里字段显示的表头名和模型一样支持函数和链接
                'title:名称',
                'cover_id|preview_pic:图标',
                'descrip:描述',
                'url:外链',
                'update_time|time_format:更新时间',
				'end_time|time_format:结束时间',
                'id:操作:[EDIT]|编辑,[DELETE]|删除'
            ),
        );

        public function install(){
            /* 先判断插件需要的钩子是否存在 */
            $this->getisHook('friendlinkDisplay', $this->info['name'], $this->info['description']);
			//读取SQL文件
			$sqldata = file_get_contents(__DIR__.'/data.sql');
            $sqlFormat = $this->sql_split($sqldata, C('DB_PREFIX'));
            $counts = count($sqlFormat);
            
            for ($i = 0; $i < $counts; $i++) {
                $sql = trim($sqlFormat[$i]);
                D()->execute($sql);
            }
            return true;
        }

        public function uninstall(){
			//删除数据表
			$md =new Model();
			$sql = 'DROP TABLE IF EXISTS `'.C('DB_PREFIX').'friendlink`';
			$md->execute($sql);
			M('hooks')->where(array('name'=>'friendlinkDisplay',))->delete();
            return true;
        }
		
		public function friendlinkDisplay($param)
		{
			$config = $this->getConfig();
			$listdb = M('friendlink')->select();
			$this->assign('addons_config',$config);
			$this->assign('listdb',$listdb);
						
			if($config['open'])
                $this->display('widget');
		}


        /**
         * 解析数据库语句函数
         * @param string $sql  sql语句   带默认前缀的
         * @param string $tablepre  自己的前缀
         * @return multitype:string 返回最终需要的sql语句
         */
        public function sql_split($sql, $tablepre) {

            if ($tablepre != "sent_")
                $sql = str_replace("sent_", $tablepre, $sql);
                $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);

            if ($r_tablepre != $s_tablepre)
                $sql = str_replace($s_tablepre, $r_tablepre, $sql);
                $sql = str_replace("\r", "\n", $sql);
                $ret = array();

                $num = 0;
                $queriesarray = explode(";\n", trim($sql));
                unset($sql);

            foreach ($queriesarray as $query) {
                $ret[$num] = '';
                $queries = explode("\n", trim($query));
                $queries = array_filter($queries);
                foreach ($queries as $query) {
                    $str1 = substr($query, 0, 1);
                    if ($str1 != '#' && $str1 != '-')
                        $ret[$num] .= $query;
                }
                $num++;
            }
            return $ret;
        }
        
        //获取插件所需的钩子是否存在
        public function getisHook($str, $addons, $msg=''){
            $hook_mod = M('Hooks');
            $where['name'] = $str;
            $gethook = $hook_mod->where($where)->find();
            if(!$gethook || empty($gethook) || !is_array($gethook)){
                $data['name'] = $str;
                $data['description'] = $msg;
                $data['type'] = 1;
                $data['update_time'] = NOW_TIME;
                $data['addons'] = $addons;
                if( false !== $hook_mod->create($data) ){
                    $hook_mod->add();
                }
            }
        }
    }