<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

return array(
	'open'=>array(
		'title'=>'是否开启:',
		'type'=>'radio',
		'options'=>array(
			'0'=>'关闭',
			'1'=>'开启',
		),
		'value'=>'1'
	),
	'width'=>array(
		'title'=>'显示宽度:',
		'type'=>'select',
		'options'=>array(
			'1'=>'1格',
			'2'=>'2格',
			'4'=>'4格'
		),
		'value'=>'2'
	),
	'display'=>array(
		'title'=>'显示内容:',
		'type'=>'radio',
		'options'=>array(
			'0'=>'友情链接',
			'1'=>'合作伙伴',
			'2'=>'显示所有'
		),
		'value'=>'2'
	),
	'frindstyle'=>array(
		'title'=>'友情链接显示方式:',
		'type'=>'radio',
		'options'=>array(
			'0'=>'图片',
			'1'=>'文字',
		),
		'value'=>'0'
	),
	'compnnystyle'=>array(
		'title'=>'合作伙伴接显示方式:',
		'type'=>'radio',
		'options'=>array(
			'0'=>'图片',
			'1'=>'文字',
		),
		'value'=>'0'
	)
);
