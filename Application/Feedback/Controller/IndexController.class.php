<?php
namespace Feedback\Controller;


class IndexController extends \Common\Controller\FrontController {

	public function index(){
		$feed = D('Feedback');
		$feedback_config = S('feedback_config');

		if ($feedback_config) {
			# code...
		}
		//$page = new \Think\Page();
		$list = $feed->where()->limit()->order()->select();
		$data = array(
			'list'  => $list,
		);

		$this->setSeo("留言反馈");
		$this->assign($data);
		$this->display();
	}

	public function add(){
		$feed = D('Feedback');
		if (IS_POST) {
			$data = $feed->create();
			if ($data) {
				$result = $feed->add();
				if ($result) {
					$this->error("提交成功！");
				}else{
					$this->error("提交失败！");
				}
			}else{
				$this->error($feed->getError());
			}
		}else{
			$this->display();
		}
	}
}