<?php
namespace Common\Controller;
use Think\Controller;

/**
 * 前台公共控制器
 * 为防止多分组Controller名称冲突，公共Controller名称统一使用分组名称
 */
class FrontController extends Controller{

    /* 空操作，用于输出404页面 */
	public function _empty(){
		$this->redirect('Index/index');
	}


	protected function _initialize(){
		/*读取站点配置*/
		$config = api('Config/lists');
		C($config); //添加配置

		if (!C('WEB_SITE_CLOSE')) {
			$this->error('站点已经关闭，请稍后访问~');
		}
		//C('DEFAULT_THEME','DDDD');
		$this->setSeo();
	}

	/* 用户登录检测 */
	protected function login(){
		/* 用户登录检测 */
		is_login() || $this->error('您还没有登录，请先登录！', U('User/login'));
	}

	protected function setSeo($title = null,$keywords = null,$description = null){
		//获取还没有经过变量替换的META信息
		$meta = D('Common/SeoRule')->getMetaOfCurrentPage();
		$seo = array(
			'title'       => $title,
			'keywords'    => $keywords,
			'description' => $description,
		);
		foreach ($seo as $key => $value) {
			if (is_array($value)) {
				foreach ($value as $k => $v) {
					$meta[$key] = str_replace("[".$k."]", $v, $meta[$key]);
				}
			}else{
				$meta[$key] = str_replace("[".$key."]", $value, $meta[$key]);
			}
		}

		$data = array(
			'title'       => $meta['title'],
			'keywords'    => $meta['keywords'],
			'description' => $meta['description'],
		);
		$this->assign($data);
	}
}