/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : tpcms

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-12-19 14:25:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sent_friendlink`
-- ----------------------------
DROP TABLE IF EXISTS `sent_friendlink`;
CREATE TABLE `sent_friendlink` (
  `id` mediumint(5) NOT NULL AUTO_INCREMENT,
  `ftype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:友情链接 1:合作单位',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '标题',
  `url` varchar(150) NOT NULL DEFAULT '' COMMENT '链接地址',
  `cover_id` int(150) NOT NULL COMMENT '封面图片ID',
  `descrip` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `list` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `ifhide` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示',
  `iswordlink` tinyint(1) DEFAULT NULL,
  `hits` tinyint(7) NOT NULL DEFAULT '0' COMMENT '点击率',
  `update_time` varchar(10) NOT NULL DEFAULT '0000-00-00' COMMENT '更新时间',
  `uid` int(7) NOT NULL DEFAULT '0' COMMENT '用户ID ',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `end_time` int(10) NOT NULL COMMENT '有效期结束时间',
  PRIMARY KEY (`id`),
  KEY `yz` (`status`,`end_time`,`ifhide`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sent_friendlink
-- ----------------------------
INSERT INTO `sent_friendlink` VALUES ('1', '0', 'THINKPHP官方网站', 'http://www.thinkphp.cn/', '1', 'THINKPHP官方论坛', '32', '0', '0', '0', '0000-00-00', '0', '1', '0');
INSERT INTO `sent_friendlink` VALUES ('2', '0', 'onthink', 'http://www.onthink.cn', '1', '国内著名的CMS建站系统提供商', '40', '0', '0', '0', '0000-00-00', '0', '1', '0');
INSERT INTO `sent_friendlink` VALUES ('3', '0', '腾速科技', 'http://www.tensent.cn', '1', '国内著名的CMS建站系统提供商', '40', '0', '0', '0', '0000-00-00', '0', '1', '0');