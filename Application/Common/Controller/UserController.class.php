<?php
namespace Common\Controller;

class UserController extends FrontController{

	public function _initialize(){
		parent::_initialize();
		$uid = intval($_REQUEST['uid']) ? intval($_REQUEST['uid']) : is_login();
		if (!$uid) {
			$this->error('需要登录',U('Ucenter/Public/login'));
		}
		$this->assign('uid', $uid);
		$this->mid = is_login();
	}


}