<?php
return array(
	'is_open'=>array(//配置在表单中的键名 ,这个会是config[random]
		'title'=>'是否开启:',	 //表单的文字
		'type'=>'select',		 //表单的类型：text、textarea、checkbox、radio、select等
		'options'=>array(		 //select 和radion、checkbox的子选项
			'1'=>'开',		 //值=>文字
			'2'=>'关',
		),
		'value'=>'1',			 //表单的默认值
	),
	'float'=>array(
		'title'=>'浮动位置',
		'type'=>'select',
		'options' => array(
			'left'=>'左浮动',		 //值=>文字
			'right'=>'右浮动',
		),
		'value'=>'right',
		'tip'=>''
	),
	'style'=>array(
		'title'=>'客服样式',
		'type'=>'select',
		'options' => array(
           'yellow' => '黄色',
           'blue' => '蓝色',
           'green' => '绿色',
           'orange' => '橘黄色',
           'gray' => '灰色',
           'white' => '白色',
		),
		'value'=>'gray',
		'tip'=>''
	),
	'qq'=>array(
		'title'=>'客服QQ',
		'type'=>'text',
		'value'=>'',
		'tip'=>'多个QQ使用英文“,”隔开，QQ号和文字之间用“|”隔开，QQ号码在前面文字在后面'
	),
	'phone'=>array(
		'title'=>'咨询热线',
		'type'=>'text',
		'value'=>'',
		'tip'=>''
	),
	'url'=>array(
		'title'=>'留言链接',
		'type'=>'text',
		'value'=>'',
		'tip'=>'留言链接'
	),
);