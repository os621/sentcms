
-- ----------------------------
-- Table structure for `sent_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `sent_feedback`;
CREATE TABLE `sent_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '留言标识',
  `title` varchar(255) NOT NULL COMMENT '留言标题',
  `name` varchar(50) NOT NULL COMMENT '留言人姓名',
  `phone` varchar(15) NOT NULL COMMENT '联系方式',
  `type` int(1) NOT NULL COMMENT '反馈类别',
  `content` text COMMENT '留言内容',
  `reply` text COMMENT '留言回复',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `create_time` int(11) NOT NULL COMMENT '留言时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `sent_menu` ( `title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `status`) VALUES
('留言配置', 93, 0, 'Feedback/index', 0, '', '留言管理', 0, 1),
('留言管理', 93, 0, 'Feedback/lists', 0, '', '留言管理', 0, 1);

set @tmp_id=0;
select @tmp_id:= id from `sent_menu` where title = '留言管理';
INSERT INTO `sent_menu` ( `title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `status`) VALUES
('回复', @tmp_id, 0, 'Feedback/edit\r', 0, '', '', 0, 0),
('删除', @tmp_id, 0, 'Feedback/delete', 0, '', '', 0, 0);